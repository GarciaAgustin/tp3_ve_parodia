using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Controller : MonoBehaviour
{

    public bool Hasendured = false;

    GameManager GM;

    private void Awake()
    {
        GM = FindAnyObjectByType<GameManager>();
    }
    void Start()
    {
        
    }

    void Update()
    {

        if(Hasendured == true && GM.SupportedTime <= 0)
        {
            GM.ShowWinPanel();
        } 
        else if(Hasendured == false && GM.SupportedTime <= 0)
        {
            GM.ShowLosePanel();
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Tetromino"))
        {
            Hasendured = true;
            GM.StartCheckingSuportedTime();

        }

        if (collision.collider.CompareTag("Floor"))
        {
            GM.StartCheckingSuportedTime();
        }

        if (collision.collider.CompareTag("Floor") && collision.collider.CompareTag("Tetromino"))
        {
            Hasendured = false;

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Floor"))
        {
            Hasendured = false;
        }

        if (collision.CompareTag("Arch"))
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            GetComponent<AudioSource>().Play();
            GM.ShowLosePanel();
        }
    }
}
