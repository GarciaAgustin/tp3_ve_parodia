using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetrominum_Controller : MonoBehaviour
{
    public Vector3 puntoDeRotacion;
    Generator_Controller generator;

    private void Awake()
    {
        generator = FindAnyObjectByType<Generator_Controller>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) { transform.position += Vector3.left; }
        if (Input.GetKeyDown(KeyCode.RightArrow)) { transform.position += Vector3.right; }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.RotateAround(transform.TransformPoint(puntoDeRotacion), new Vector3(0, 0, 1), -90); 
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("TetrominoFloor") || collision.collider.CompareTag("Tetromino"))
        {
            
            this.enabled = false; 
            generator.NuevoTetronimun(); 
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Floor"))
        {
         
            this.enabled = false;
            generator.NuevoTetronimun();
        }
    }
}