using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Controller : MonoBehaviour
{

    private void Awake()
    {
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Menu();
        }
    }
    public void Car()
    {
        SceneManager.LoadScene("Car");
    }
    public void Football()
    {
        SceneManager.LoadScene("Football");
    }
    public void Duck()
    {
        SceneManager.LoadScene("Duck");
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
