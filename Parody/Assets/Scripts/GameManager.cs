using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    //public static GameManager instance;

    [SerializeField]
    GameObject Platform;

    [SerializeField]
    TextMeshProUGUI PiecesTXT;

    [SerializeField]
    TextMeshProUGUI SupportedTimeTXT;

    public float SupportedTime;

    public bool StartsubtractingTime = false;

    [SerializeField]
    bool IsWatherLevel;
    [SerializeField]
    bool IsCarLevel;
    public bool IsFootballLevel;

    [SerializeField]
    GameObject Object;

    bool objectForceApplied = false;

    public GameObject WinAndLosePanel;
    Color WinOrLosePanelColor;

    Vector3 ObjectPos;

    Generator_Controller generator;

    private void Awake()
    {
        //if (instance == null)
        //    instance = this;
        //else
        //{
        //    Destroy(gameObject);
        //    return;
        //}

        //DontDestroyOnLoad(gameObject);

        ObjectPos = Object.transform.position;
        generator = FindAnyObjectByType<Generator_Controller>();
    }

    void Start()
    {
        
    }

   
    void Update()
    {
        PiecesTXT.text = "Piezas: " + generator.Pieces.ToString();

        if(generator.Pieces <= 0)
        {
            StartCoroutine(WaitForUseObject());
        }

        if(SupportedTime <= 0)
        {
            SupportedTime = 0;
        }

        if(IsCarLevel == true)
        {
            SupportedTimeTXT.text = "Evita que toque el suelo por: " + (int)SupportedTime;
        }
        else if(IsWatherLevel == true)
        {
            SupportedTimeTXT.text = "Evita que se moje por: " + (int)SupportedTime;
        }
        else if (IsFootballLevel == true)
        {
            SupportedTimeTXT.text = "Evita que se metan gol por: " + (int)SupportedTime;
        }

        if (StartsubtractingTime == true)
        {
            SupportedTime -= Time.deltaTime;
        }
    }

    IEnumerator WaitForUseObject()
    {
        yield return new WaitForSeconds(5);
        Platform.SetActive(false);

        if (IsFootballLevel == true && objectForceApplied == false)
        {
            Object.GetComponent<Rigidbody2D>().AddForce(Vector3.right * 200, ForceMode2D.Impulse);
            objectForceApplied = true;
            StartsubtractingTime = true;
        }
    }

    public void StartCheckingSuportedTime()
    {
        SupportedTimeTXT.gameObject.SetActive(true);
        StartsubtractingTime = true;
    }

    public void ShowWinPanel()
    {
        ColorUtility.TryParseHtmlString("#4DA454", out WinOrLosePanelColor);

        WinAndLosePanel.GetComponent<Image>().color = WinOrLosePanelColor;
        WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Win";
        WinAndLosePanel.gameObject.SetActive(true);

    }

    public void ShowLosePanel()
    {
        if (WinAndLosePanel != null)
        {
            ColorUtility.TryParseHtmlString("#9C2424", out WinOrLosePanelColor);

            WinAndLosePanel.GetComponent<Image>().color = WinOrLosePanelColor;
            WinAndLosePanel.GetComponentInChildren<TextMeshProUGUI>().text = "You Lose";
            WinAndLosePanel.gameObject.SetActive(true);
        }
    }

    public void Restart()
    {
        //foreach (GameObject t in Generator_Controller.instance.Active_tetronimus)
        //{
        //    Destroy(t);
        //}
        //Generator_Controller.instance.Active_tetronimus.Clear();
        //Generator_Controller.instance.Pieces = 1;
        //Platform.SetActive(true);
        //Object.transform.position = ObjectPos;
        //WinAndLosePanel.gameObject?.SetActive(false);
        //StartsubtractingTime = false;
        //objectForceApplied = false;
        //SupportedTime = 7;
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }
}