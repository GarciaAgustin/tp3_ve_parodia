using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator_Controller : MonoBehaviour
{
    public GameObject[] tetronimus;
    //public static Generator_Controller instance;
    public List<GameObject> Active_tetronimus = new List<GameObject>();

    public int Pieces;

    private void Awake()
    {
        //if (instance == null)
        //{
        //    instance = this;
        //    DontDestroyOnLoad(gameObject);
        //}
        //else
        //{
        //    Destroy(gameObject);
        //}

        NuevoTetronimun();
    }

    public void NuevoTetronimun()
    {
         AreAllScriptsDisabled();

        if (AreAllScriptsDisabled() && Pieces > 0)
        {
            Pieces -= 1;
            GameObject t = Instantiate(tetronimus[Random.Range(0, tetronimus.Length)], transform.position, Quaternion.identity);
            Active_tetronimus.Add(t);
        }
    }

    public bool AreAllScriptsDisabled()
    {
        foreach (GameObject tetromino in Active_tetronimus)
        {
            Tetrominum_Controller script = tetromino.GetComponent<Tetrominum_Controller>(); 
            if (script != null && script.enabled) 
            {
                return false; 
            }
        }
        return true; 
    }
}
